//
//  AppDelegate.h
//  BLE
//
//  Created by Mac Mav on 20/12/2016.
//  Copyright © 2016 Mac Mav. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

