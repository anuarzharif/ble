//
//  ViewController.m
//  BLE
//
//  Created by Mac Mav on 20/12/2016.
//  Copyright © 2016 Mac Mav. All rights reserved.
//

#import "ViewController.h"

#import "BeaconReceiver.h"

@interface ViewController ()
{
    NSDictionary *dictBeaconData;
}

@end

@implementation ViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setVariable];
    [self setGraphic];
}

-(void)setVariable
{
//    NSString *strgUUID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"A77A1B68-49A7-4DBF-914C-760D07FBB87B"];
    
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
                                                                  major:1
                                                                  minor:1
                                                             identifier:@"com.hatching.testregion"];
    
//    NSUUID *myDevice = [NSUUID UUID];
//    NSString *deviceUDID = myDevice.UUIDString;
//    
//    NSLog(@"testing %@", deviceUDID);
    
//    UIDevice *myDevice = [UIDevice currentDevice];
//    NSString *deviceUDID = myDevice.uniqueIdentifier;
//    NSString *deviceName = myDevice.Name;
//    NSString *deviceSystemName = myDevice.systemName;
//    NSString *deviceOSVersion = myDevice.systemVersion;
//    NSString *deviceModel = myDevice.model;
//    
//    In iOS 6 to get UDID:
//    
    
//    NSUUID *deviceId;
//#if TARGET_IPHONE_SIMULATOR
//    deviceId = [NSUUID initWithUUIDString:@"UUID-STRING-VALUE"];
//#else
//    deviceId = [UIDevice currentDevice].identifierForVendor;
//    
//    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"A77A1B68-49A7-4DBF-914C-760D07FBB87B"];
//    
//    // Initialize the Beacon Region
//    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
//                                                                  major:1
//                                                                  minor:1
//                                                             identifier:@"com.appcoda.testregion"];
}

-(void)setGraphic
{
    self.title = @"BLE Broadcast";
}

#pragma mark - button section
-(IBAction)btnDisplayTapped:(id)sender
{
//    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
//    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], CBCentralManagerScanOptionAllowDuplicatesKey, nil];
//    
//    [self.centralManager scanForPeripheralsWithServices:nil options:options];
//    self.centralManager.delegate = self;
    
    
    dictBeaconData = [self.myBeaconRegion peripheralDataWithMeasuredPower:nil];
    
    self.peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self
                                                                     queue:nil
                                                                   options:nil];
    
    self.peripheralManager.delegate = self;
}

-(IBAction)btnReceiverTapped:(id)sender
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BeaconReceiver *noname = [storyBoard instantiateViewControllerWithIdentifier:@"BeaconReceiver"];
    [self showViewController:noname sender:self];
}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"centralManagerDidUpdateState %@", central);
}

#pragma mark - peripheralmanager section
-(void)peripheralManagerDidUpdateState:(CBPeripheralManager*)peripheral
{
    if (peripheral.state == CBPeripheralManagerStatePoweredOn)
    {
        // Bluetooth is on
        
        // Update our status label
        lblDisplay.text = @"Broadcasting...";
        
        // Start broadcasting
        [self.peripheralManager startAdvertising:dictBeaconData];
    }
    else if (peripheral.state == CBPeripheralManagerStatePoweredOff)
    {
        // Update our status label
        lblDisplay.text = @"Stopped";
        
        // Bluetooth isn't on. Stop broadcasting
        [self.peripheralManager stopAdvertising];
    }
    else if (peripheral.state == CBPeripheralManagerStateUnsupported)
    {
        lblDisplay.text = @"Unsupported";
    }
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
