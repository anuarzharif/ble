//
//  BeaconReceiver.h
//  BLE
//
//  Created by Mac Mav on 20/12/2016.
//  Copyright © 2016 Mac Mav. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>

@interface BeaconReceiver : UIViewController<CLLocationManagerDelegate>
{
    IBOutlet UILabel *lblDisplay;
}

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end
