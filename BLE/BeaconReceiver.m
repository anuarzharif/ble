//
//  BeaconReceiver.m
//  BLE
//
//  Created by Mac Mav on 20/12/2016.
//  Copyright © 2016 Mac Mav. All rights reserved.
//

#import "BeaconReceiver.h"

@interface BeaconReceiver ()

@end

@implementation BeaconReceiver

-(void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setVariable];
    [self setGraphic];
}

-(void)setVariable
{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    
//    NSString *strgUUID = [UIDevice currentDevice].identifierForVendor.UUIDString;
//    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"A77A1B68-49A7-4DBF-914C-760D07FBB87B"];
//    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid
//                                                             identifier:@"com.hatching.testregion"];
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:@"98:D3:31:F4:13:BD"];
    self.myBeaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:1 minor:1 identifier:@"com.hatching.testregion"];
    
    
    // Tell location manager to start monitoring for the beacon region
    [self.locationManager startMonitoringForRegion:self.myBeaconRegion];
}

-(void)setGraphic
{
    self.title = @"BLE Receiver";
}

#pragma mark - locationmanager section
- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    [self.locationManager startRangingBeaconsInRegion:self.myBeaconRegion];
}

-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    [self.locationManager stopRangingBeaconsInRegion:self.myBeaconRegion];
    lblDisplay.text = @"No";
}

-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
    lblDisplay.text = @"Beacon found!";
    CLBeacon *foundBeacon = [beacons firstObject];
    NSLog(@"testing receive: %@", foundBeacon);
    
    // You can retrieve the beacon data from its properties
    //NSString *uuid = foundBeacon.proximityUUID.UUIDString;
    //NSString *major = [NSString stringWithFormat:@"%@", foundBeacon.major];
    //NSString *minor = [NSString stringWithFormat:@"%@", foundBeacon.minor];
}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
