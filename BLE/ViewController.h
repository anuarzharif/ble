//
//  ViewController.h
//  BLE
//
//  Created by Mac Mav on 20/12/2016.
//  Copyright © 2016 Mac Mav. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController : UIViewController<CBPeripheralManagerDelegate, CBCentralManagerDelegate>
{
    IBOutlet UIButton *btnDisplay;
    
    IBOutlet UILabel *lblDisplay;
}

@property (strong, nonatomic) CLBeaconRegion *myBeaconRegion;
@property (strong, nonatomic) CBPeripheralManager *peripheralManager;

@property (strong, nonatomic) CBCentralManager *centralManager; //ak tmbah

@end

